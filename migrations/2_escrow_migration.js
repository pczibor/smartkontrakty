const { default: Web3 } = require("web3");
const EscrowFactory = artifacts.require("EscrowFactory");

module.exports = function(deployer, network, accounts) {
  deployer.deploy(
    EscrowFactory,
    {
      from: accounts[0],
    }
  );
};