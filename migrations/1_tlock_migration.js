const { default: Web3 } = require("web3");
const tlock = artifacts.require("TimeLock");

module.exports = function(deployer, network, accounts) {
    deployer.deploy(
        tlock,
        "Trustless fund",
        {
            from: accounts[0],
        }
    )
}