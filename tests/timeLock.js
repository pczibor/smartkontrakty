const { assert, expect } = require('chai')
const TimeLock = artifacts.require("TimeLock")
require('web3')
require('chai')
    .use(require('chai-as-promised'))
    .should()


// sleep time expects milliseconds
function sleep (time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}

Accounts = web3.eth.getAccounts()
contract('TimeLock', (Accounts) => {
    let tlock
    let a
    const sleep = ms => new Promise(res => setTimeout(res, ms));


    before(async () => {
        a = Accounts
        tlock = await TimeLock.deployed({from: a[5]})
    })

    // create contract & check balance
    describe("TimeLock deployment", async () => {
        it("it's deployed and locked", async () => {
             const name = await tlock.name();
            const state = await tlock.state()
            const locked = web3.utils.fromWei(await tlock.getBalance());
            assert.equal(name, "Ma xmass gift")
            assert.equal(state, 0)
            assert.equal(locked, 10)
        })
    })

    // try to withdraw before timeout
    describe("withdraw before timelock passed", async () => {
        it("not possible to withdraw", async () => {
            await console.log(a[5])
            //const locked = tlock.withdraw( {from: a[5]})
            const state = await tlock.state()
            try {
                await tlock.withdraw( {from: a[5]})
                assert.fail()
            } catch (error) {
                assert(error.toString(), error.toString())
            }
            assert.equal(state, 0)
        })
    })
    
    // try to withdraw after timeot
    describe("withdraw after timelock passed", async () => {
        it("now you can withdraw", async () => {
            await sleep(90000)

            old_state = await tlock.state()
            await tlock.withdraw({from: a[5]})
            state = await tlock.state()
            console.log("old state: " + old_state)
            console.log("new state: " + state)
            assert.equal(old_state, 0)
            assert.equal(state, 1)
        })
    })
})