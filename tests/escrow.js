const { assert } = require('chai')
const Escrow = artifacts.require("Escrow")
require('web3')
require('chai')
    .use(require('chai-as-promised'))
    .should()

contract('Escrow', (Accounts) => {
    let escrow

    // runs before all the rest
    before(async () => {
        escrow = await Escrow.deployed()
    })

    // test if escrow is deployed
     
    describe("Escrow deployment", async () => {
        it('has a name', async () => {
            const name = await escrow.name();
            assert.equal(name, "Apple")
        })
    })

    // test escrow state & sellers deposit
    describe("Escrow state after deployment", async () => {
        it('has a state and eth locked in', async () => {
            const locked = web3.utils.fromWei(await escrow.getSellerDeposit());
            const state = await escrow.state();
            assert.equal(locked, 20)
            assert.equal(state, 1)
        })
    })

    // deposit money as buyer
    describe("Buyer deposit", async () => {
        it("buyer made his deposit", async () => {
            escrow.buyerDeposit({from: Accounts[3], value: web3.utils.toWei("20", "ether")})
            const locked = web3.utils.fromWei(await escrow.getSellerDeposit());
            assert.equal(locked, 20)
        })
    })

    // escrow state
    describe("Escrow state after buyer deposit", async () => {
        it("state changed to await confirmation", async () => {
            const state = await escrow.state()
            assert.equal(state, 2)
        })
    })

    // buyer confirms goods
    describe("buyer has confirmed stuff arrived", async () => {
        it("state changed to confirmed", async () => {
            await escrow.confirmation({from: Accounts[3]})
            const state = await escrow.state()
            assert.equal(state, 3)
        })
    })

    // seller completes trade
    describe("seller refounds himself", async () => {
        it("seller got money and deposit", async () => {
            await escrow.refoundSeller({from: Accounts[0]})
            const state = await escrow.state()
            assert.equal(state, 4)
        })
    })
})