import React, { Component } from 'react'

class Navbar extends Component {

  render() {
    const sttable = {
      borderBottomLeftRadius: '25px',
      borderBottomRightRadius: '25px',
    }
    return (
      <nav className="navbar navbar-dark bg-dark" style={sttable}>
        <div className="container-fluid">
            <ul className="nav justify-content-center">
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="/">Home</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/funds">Fund</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/escrow">Escrow</a>
              </li>
            </ul>
        </div>
    </nav>
    );
  }
}

export default Navbar;