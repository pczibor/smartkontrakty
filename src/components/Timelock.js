import React, { Component } from 'react'


class Timelock extends Component {
    render() {
      const stleft = {textAlign:'left'};
      const stcenter = {textAlign:'center'};
      const stright = {textAlign:'right'};
      const stfloatr = {float: 'right'};

        return (
          
<div>
  <div className="row">
    <div className="col-sm"></div>
    <div className="col-sm">
      <table className="table text-muted text-center">
        <tbody>
          <tr>
            <td style={stleft} ><b>Address</b></td>
            <td style={stcenter} ><p>{this.props.account}</p></td>
          </tr>
          <tr>
            <td style={stleft} ><b>Address Balance</b></td>
            <td style={stcenter} id="mark1"><p>{this.props.balance}</p></td>
            <td style={stright}><span className="input-group-text" id="mark1" >ETH</span></td>
          </tr>
          <tr>
            <td style={stleft}><b>Locked Balance</b></td>
            <td style={stcenter}>{this.props.tlockBalance} ETH </td>
            <td style={stright}>
              <span className="input-group-text" id="mark1" >ETH</span>
            </td>
            <td>
              <button 
                type="submit" 
                className="btn btn-dark btn-block btn-sm"
                onClick={(event) => {
                  event.preventDefault()
                  this.props.fundwithdraw()
                }}

              >   Withdraw
              </button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div className="col-sm"></div>
  </div>

  <div className="row">
    <div className="col-md"></div>
    <div className="col-md">
      <div className="card mb-4" >
        <div className="card-body">
          <form className="mb-3"  onSubmit={(event) => {
                    event.preventDefault()
                    let amount, time
                    amount = this.amount.value.toString()
                    time = this.time.value.toString()
                    if (amount === "" || time === "") {
                      window.alert("Wrong input");
                      return;
                    }
                    amount = window.web3.utils.toWei(amount, 'Ether')
                    this.props.fundlocker(amount, time)
                    
                  }}
          >
            <h3>Lock funds</h3>

            <div className="input-group mb-3">
              <input type="text" className="form-control" placeholder="Amount"  ref={(amount) => { this.amount = amount }}/>
              <span className="input-group-text" id="basic-addon1">ETH</span>
            </div>
                          
            <div className="input-group mb-3">
              <input type="text" className="form-control" placeholder="Lock time in seconds" ref={(time) => {this.time = time}}/>
              <span className="input-group-text" id="basic-addon2">SEC</span>
          </div>
            <button style={stfloatr} type="submit" className="btn btn-dark btn-block btn-sm">Lock funds</button>
          </form>
        </div>
      </div>    
    </div>
    <div className="col-md"></div>
  </div>
</div>
        )
    }
}

export default Timelock