import React, { Component } from 'react'
import { withRouter } from "react-router";

class EscrowInit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            factory: this.props.factory,
            escrowName: '',
        }
    }
    
    async initEscrow(name) {
        this.props.parent.setState({ loading: true })
        const contractInstance = this.props.factory;

        contractInstance.methods.initEscrow(name).send({from: this.props.account})
            .on('transactionHash', function(hash){console.log("thash")})
            .on('confirmation', function(confNumber, receipt){console.log("conf")})
            .on('receipt', function(receipt){
                let id = receipt.events.escrowCreated.returnValues.id;
                window.location.href = "/escrow/" + id;
                this.props.parent.setState({ loading: false})
            })
            .on('error', function(error, receipt){console.log("err")})
    }

    _setEscrowAddress = (address) => {
        this.setState({escrowAddr: address})
    }
    
    _setEscrowId = (id) => {
        this.setState({escrowId: id})
    }
    
    _setEscrowName = (name) => {
        this.setState({name: name})
    }

    render () {
        let content =
        <div className="row">
            <div className="col-md"></div>
            <div className="col-md">
                <div className="card mb-4" >
                    <div className="card-body">
                        <form className="mb-3" onSubmit={(event) => {
                            event.preventDefault()
                            if (this.name && this.name.value === "") {
                                window.alert("Wrong input");
                                return;
                            }
                            this.initEscrow(this.name.value.toString())
                            }}>
                            <h3>Create escrow</h3>

                            <div className="input-group mb-3">
                                <input type="text" className="form-control" ref={(name) => {this.name = name}}/>
                            </div>
                            
                            <button style={{float:"right"}} type="submit" className="btn btn-dark btn-block btn-sm">Create</button>
                        </form>
                    </div>
                </div>    
            </div>
            <div className="col-md"></div>
        </div>
        return (
            <div>{content}</div>
        );
    }
}

class EscrowDeposit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            deposit: ''
        }
    }

    accept = () => {
        this.setState({deposit: 'accepted'})
    }

    deny = () => {
        this.setState({deposit: 'denied'})
    }
        
    async deposit(amount) {
        this.props.parent.setState({ loading: true })
        const contractInstance = this.props.escrow;

        const _deny = this.deny;

        let func;
        if (1 === this.props.escrowState) {
            func = contractInstance.methods.sellerDeposit;
        } else if (2 === this.props.escrowState) {
            func = contractInstance.methods.buyerDeposit;
        } else {
            _deny();
            return;
        }
        amount = window.web3.utils.toWei(amount, 'Ether')
        func().send({from: this.props.account, value: amount})
            .once('transactionHash', function(hash){})
            .on('confirmation', function(confNumber, receipt){ })
            .on('receipt', function(receipt){
                window.location.reload();        
             })
            .on('error', function(error){})
        this.props.parent.setState({ loading: false})
    }

    async abort() {
        const contractInstance = this.props.escrow;
        let func = contractInstance.methods.abort;
        func().send({from: this.props.account})
            .on('error', function(error){
                window.alert("Contract can not be aborted.")
            })
            .on('receipt', function(receipt) {
                window.location.reload();
            })
    }

    render() {
        let deposit_type;
        if (1 === this.props.escrowState) {
            deposit_type =  <h1> Deposit as seller </h1>
        } else if (2 === this.props.escrowState) {
            deposit_type =  <h1> Deposit as  buyer </h1>
        } else {
            deposit_type = <h1>Deposit not available</h1>
        }
        let content = 
        <div className="row">
            <div className="col-md"></div>
            <div className="col-md">
                <div className="card mb-4" >
                    <div className="card-body">
                        <form className="mb-3"  onSubmit={(event) => {
                            event.preventDefault()
                            if (this.amount && this.amount.value === "") {
                                window.alert("Wrong input");
                                return;
                            }
                            this.deposit(this.amount.value.toString())
                            }}>
                            <h3>{deposit_type}</h3>

                            <div className="input-group mb-3">
                                <input type="text" className="form-control" ref={(amount) => {this.amount = amount}}/>
                            </div>
                        
                            <button style={{float:"right"}} type="submit" className="btn btn-dark btn-block btn-sm">Deposit</button>
                            {2 === this.props.escrowState && 
                                <button style={{float:"left"}} type="submit" className="btn btn-danger btn-block btn-sm" onClick={
                                    (event) => {
                                        event.preventDefault()
                                        this.abort()
                                    }
                                }>Abort</button>
                            }
                        </form>
                    </div>
                </div>    
            </div>
            <div className="col-md"></div>
        </div>

        return (
            <div>
                {content}
            </div>
        )
    }
}


class EscrowApproval extends Component {
    constructor(props) {
        super(props);
        this.state = {
            factory: this.props.factory,
            deposit: '',
            approveas: '' 
        }
    }

    async componentDidMount() {
        await this.approvalAs();
    }

    async approvalAs() {
        console.log(this.props)
        let seller = await this.props.escrow.methods.sellerConfirm().call()
        let buyer = await this.props.escrow.methods.buyerConfirm().call()

        if (seller === false && buyer === false) {
            this.setState( {approveas: 'seller or buyer'})
        } else if (seller === false && buyer === true) {
            this.setState( {approveas: 'seller'})
        } else if ((buyer === false && seller === true)) {
            this.setState( {approveas: 'buyer'})
        }

    }

    confSeller = () => {
        console.log("Confirmed as seller");
    }

    confBuyer = () => {
        console.log("Confirmed as buyer");
    }

    async approve() {
        this.props.parent.setState({ loading: true })
        const contractInstance = this.props.escrow;

        if (3 !== this.props.escrowState) {
            return;
        }

        const func = contractInstance.methods.confirmation;
        
        func().send({from: this.props.account})
        .on('error', function(error){})
        .on('receipt', function(receipt){ 
            window.location.reload();
        })
        
        this.props.parent.setState({ loading: false})
    }

    render() {
        let content;

        if ( 3 === this.props.escrowState) {
            content =
            <div className="row">
            <div className="col-md"></div>
            <div className="col-md">
                <div className="card mb-4" >
                    <div className="card-body">
                        <form className="mb-3">
                            <h3>Approval as {this.state.approveas}</h3>                            
                            <button style={{float:"right"}} type="submit" className="btn btn-dark btn-block btn-sm text-center"
                                    onClick={() => {
                                        this.approve();
                                    }}
                            >Approve</button>
                        </form>
                    </div>
                </div>    
            </div>
            <div className="col-md"></div>
        </div>


        } else if ( 4 === this.props.escrowState) {
            content = 
                <h3>This escrow has alreade been approved </h3>
        } else {
            content = 
                <h3 >This escrow can not yet be approved</h3>
        }

        return (
            <div>
                {content}
            </div>
        )
    }
}

class EscrowWithdrawal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            factory: this.props.factory,
        }
    }

    async withdraw() {
        this.props.parent.setState({ loading: true })
        const contractInstance = this.props.escrow;

        const func = contractInstance.methods.withdraw;
        func().send({from: this.props.account})
        .on('receipt', function(receipt){
            window.location.reload();
         })
        .on('error', function(error){})
        this.props.parent.setState({ loading: false})
        window.location.reload();
    }

    render() {
        
        let content;
        if (4 === this.props.escrowState ) {
            content =
            <div className="row">
            <div className="col-md"></div>
            <div className="col-md">
                <div className="card mb-4" >
                    <div className="card-body">
                        <form className="mb-3">
                            <h3>Withdraw</h3>                            
                            <button style={{float:"right"}}type="submit" className="btn btn-dark btn-block btn-sm text-center"
                                onClick={() => {
                                    this.withdraw();
                                }}
                            >Withdraw</button>
                        </form>
                    </div>
                </div>    
            </div>
            <div className="col-md"></div>
        </div>

        } else {
            content = 
                <h1>CAN NOT WITHDRAW</h1>
        }
        return (
            <div>
                {content}
            </div>
        )
    }
    
}

class EscrowDone extends Component {
    constructor(props) {
        super(props);
        this.state = {
            factory: this.props.factory,
        }
    }

    render() {
        
        let content;
        content =
            <div className="row">
            <div className="col-md"></div>
            <div className="col-md">
                <div className="card mb-4" >
                    <div className="card-body">
                        <h2>This escrow is innactive</h2>
                    </div>
                </div>    
            </div>
            <div className="col-md"></div>
        </div>

        return (
            <div>
                {content}
            </div>
        )
    }
    
}

const E_EscrowInit = withRouter(EscrowInit);
const E_EscrowDeposit = withRouter(EscrowDeposit);
const E_EscrowApproval = withRouter(EscrowApproval);
const E_EscrowWithdrawal = withRouter(EscrowWithdrawal);
const E_EscrowDone = withRouter(EscrowDone)

export {E_EscrowInit, E_EscrowDeposit, E_EscrowApproval, E_EscrowWithdrawal, E_EscrowDone}

