import React, { Component } from 'react'
import Web3 from 'web3'
import Navbar from './Navbar';
import EscrowFactory from '../abis/EscrowFactory.json'
import Escrow from '../abis/Escrow.json'

class App extends Component {
    async componentDidMount() {
        await this.loadWeb3()
        await this.loadBchainData()
    }
   
    setStateAsync(state) {
        return new Promise((resolve) => {
          this.setState(state, resolve)
        });
    }
    
    async loadWeb3() {
        if (window.ethereum) {
            window.web3 = new Web3(window.ethereum)
            await window.ethereum.enable()
        } else if (window.web3) {
            window.web3 = new Web3(window.web3.currentProvider)
        } else {
            window.alert('Non-Ethereum browser detected')
        }        
    }

    async loadBchainData() {
        const web3 = window.web3
        const accounts = await web3.eth.getAccounts()
        this.setStateAsync( {account: accounts[0]} )
        let balance =  web3.utils.fromWei(await web3.eth.getBalance(accounts[0]), 'Ether')
        this.setState(  {accountBalance: balance} )

        const networkId = await web3.eth.net.getId()
        const escrowFactoryData = await EscrowFactory.networks[networkId]
        if(escrowFactoryData) {
            const escrowFactory = await new web3.eth.Contract(EscrowFactory.abi, escrowFactoryData.address)
            this.setState({ escrowFactory: escrowFactory})
        } else {
            window.alert('Escrow contract is not deployed!')
        }

        const _loading = false
        this.setState({loading: _loading})
    }

    initEscrow = (name) => {
        this.setState({ loading: true })
        let ret = this.state.escrowFactory.methods.initEscrow(name).send({from: this.state.account})
        this.setState({ loading: false})
        console.table(ret)
    }

    getEscrowAddress = (id) => {
        this.setState({ loading: true })
        let addr = this.state.escrowFactory.methods.getEscrowAddress(id).call()
        this.setState({ loading: false})
        return addr
    }

    constructor(props) {
        super(props)
        this.state = {
            account: '0x0',
            accountBalance: NaN,
            escrowFactory: {},
            escrow: {},
            escrowState: '',
            escrowAddress: '',
            loading: true,

        }
    }

    render() {
        let content = ""
        if (this.state.loading === true) {
            content = <h1>Loading ... </h1> 
        }

        if (this.state.loading === false) {
            content = <div>
                    <form className="mb-3" onSubmit={(event) => {
                        event.preventDefault()
                        this.initEscrow("Predaj pixel4")
                
                    }}>
                        <button type="submit" className="btn btn-primary btn-block btn-lg">Lock funds</button>
                    </form>
                    <form className="mb-3" onSubmit={(event) => {
                        event.preventDefault()
                
                    }}>
                        <button type="submit" className="btn btn-primary btn-block btn-lg">Gimme escrow address</button>
                    </form>

                    <form className="mb-3" onSubmit={(event) => {
                        event.preventDefault()
                        console.table(this.state)
                
                    }}>
                        <button type="submit" className="btn btn-primary btn-block btn-lg">dump state vars</button>
                    </form>
                
                </div>
            
        }

        return (
            <div>
                <Navbar account={this.state.account}/>
                <div className="container-fluid mt-5">
                    <div className="row">
                        <main role="main" className="col-lg-12 ml-auto mr-auto" >
                            <div className="content mr-auto ml-auto">
                              {content}
                            </div>
                        </main>
                    </div>
                </div>
            </div>
        )
    }
}

export default App;