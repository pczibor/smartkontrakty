import React, { Component } from 'react'
import Web3 from 'web3'
import Timelock from '../components/Timelock'
import TimeLock from '../abis/TimeLock.json'


class TimeLockBootstrap extends Component {
    async componentDidMount() {
        await this.loadWeb3()
        await this.loadBchainData()
    }
   
    setStateAsync(state) {
        return new Promise((resolve) => {
          this.setState(state, resolve)
        });
    }
    
    async loadWeb3() {
        if (window.ethereum) {
            window.web3 = new Web3(window.ethereum)
            await window.ethereum.enable()
        } else if (window.web3) {
            window.web3 = new Web3(window.web3.currentProvider)
        } else {
            window.alert('Non-Ethereum browser detected')
        }        
    }

    async loadBchainData() {
        const web3 = window.web3
        const accounts = await web3.eth.getAccounts()
        this.setStateAsync( {account: accounts[0]} )
        let balance = await web3.eth.getBalance(accounts[0])
        balance = web3.utils.fromWei(balance, 'Ether')
        this.setState(  {accountBalance: balance} )

        const networkId = await web3.eth.net.getId()
        const tlockData = await TimeLock.networks[networkId]
        if(tlockData) {
            const tlock = await new web3.eth.Contract(TimeLock.abi, tlockData.address)
            this.setState({ tlock: tlock })
            const locked = String(await tlock.methods.getBalance().call({ from: this.state.account}));
            this.setState({tlockBalance: window.web3.utils.fromWei(locked)})
        } else {
            window.alert('Time lock contract is not deployed to ETH network')
        }

        const _loading = false
        this.setState({loading: _loading})
    }

    fundlocker = (amount, time) => {
        this.setState({ loading: true })
        this.state.tlock.methods.lockFunds(time).send({ from: this.state.account, value: amount }).on('transactionHash', function(hash) {
            window.location.reload();
        })
    }

    fundWithdraw = (id) => {
        this.setState({loading: true})
        this.state.tlock.methods.withdraw().send({from:this.state.account}).on('transactionHash', function(hash) {
            window.location.reload();
        })
    }

    constructor(props) {
        super(props)
        this.state = {
            account: '0x0',
            accountBalance: NaN,
            tlock: {},
            tlockBalance: '0',
            loading: true,
        }
    }

    render() {
        let content = ""
        if (this.state.loading === true) {
            content = <h1>Loading ... </h1> 
        }

        if (this.state.loading === false) {
            content = <Timelock
                tlock={this.state.tlock}
                account={this.state.account}
                balance={this.state.accountBalance}
                tlockBalance={this.state.tlockBalance}
                fundlocker={this.fundlocker}
                fundwithdraw={this.fundWithdraw}
            />
        }

        return (
            <div>
                <div className="container-fluid">
                    {content}
                </div>
            </div>
        )
    }
}

export default TimeLockBootstrap;