import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import '../model/Web3loader'
import TimeLockBootstrap from "./Funds";
import EscrowBootstrap from "./Escrow";
import Navbar from "../components/Navbar";

export default function App() {
  return (
    <Router>
      <Navbar />
      <div>
        <Switch>
          <Route path="/funds" component={TimeLockBootstrap}></Route>
          <Route path="/escrow/:id" component={EscrowBootstrap}></Route>
          <Route path="/escrow" component={EscrowBootstrap}></Route>
          <Route path="/"><Home /></Route>
        </Switch>
      </div>
    </Router>
  );
}

function Home() {
  return <h2>Home</h2>;
}