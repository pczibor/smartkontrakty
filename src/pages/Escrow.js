import React, { Component } from 'react'
import Web3 from 'web3'
import {
    E_EscrowInit,
    E_EscrowDeposit,
    E_EscrowApproval,
    E_EscrowWithdrawal,
    E_EscrowDone
} from '../components/Escrow.js'

import Escrow from '../abis/Escrow.json'
import EscrowFactory from '../abis/EscrowFactory.json'


class EscrowBootstrap extends Component {
    async componentDidMount() {
        await this.loadWeb3()
        await this.loadBchainData()
        await this.loadEscrowData()
    }
   
    setStateAsync(state) {
        return new Promise((resolve) => {
          this.setState(state, resolve)
        });
    }
    
    async loadWeb3() {
        if (window.ethereum) {
            window.web3 = new Web3(window.ethereum)
            await window.ethereum.enable()
        } else if (window.web3) {
            window.web3 = new Web3(window.web3.currentProvider)
        } else {
            window.alert('Non-Ethereum browser detected')
        }        
    }

    async loadBchainData() {
        const web3 = window.web3
        const accounts = await web3.eth.getAccounts()
        this.setStateAsync( {account: accounts[0]} )
        let balance =  await web3.eth.getBalance(accounts[0])
        balance = web3.utils.fromWei(balance, 'Ether')
        this.setState(  {accountBalance: balance} )

        const networkId = await web3.eth.net.getId()
        const escrowFactoryData = await EscrowFactory.networks[networkId]

        if(escrowFactoryData) {
            const ef = await new web3.eth.Contract(EscrowFactory.abi, escrowFactoryData.address)
            this.setState({ escrowFactory: ef})
        } else {
            window.alert('Escrow contract is not deployed!')
        }

        const _loading = false
        this.setState({loading: _loading})
    }

    async loadEscrowData() {
        const contractInstance = this.state.escrowFactory;

        if (this.props.match.params.id === undefined) {
           return;
        } else {
            const id = this.props.match.params.id;
            let address = await contractInstance.methods.getEscrowAddress(id).call()
            let contract = await new window.web3.eth.Contract(Escrow.abi, address)
            let state = await contract.methods.state().call()
            let name = await contract.methods.name().call()

            this.setState( {escrowId: id} )
            this.setState( {escrowName: name} )
            this.setState( {escrow:contract} )
            this.setState( {escrowAddress:address} )
            this.setState( {escrowState:state} )
        }
    }

    initEscrow = (name) => {
        this.setState({ loading: true })
        let ret = this.state.escrowFactory.methods.initEscrow(name).send({from: this.state.account})
        this.setState({ loading: false})
        console.table(ret)
    }

    getEscrowAddress = (id) => {
        this.setState({ loading: true })
        let addr = this.state.escrowFactory.methods.getEscrowAddress(id).call()
        this.setState({ loading: false})
        return addr
    }

    constructor(props) {
        super(props)
        this.state = {
            account: '0x0',
            accountBalance: '',
            escrowFactory: '',
            escrow: '-',
            escrowId: '-',
            escrowName: '-',
            escrowState: '-',
            escrowAddress: '-',
            loading: true,
        }
    }

    render() {
        const stleft = {textAlign:'left'};
        const stcenter = {textAlign:'center'};


        let content = ""
        if (this.state.loading === true) {
            content = <h1>Loading ... </h1> 
        }

        if (this.state.loading === false) {                
            if (this.props.match.params.id === undefined) {
                content = 
                <div>
                    <E_EscrowInit
                        factory={this.state.escrowFactory}
                        account={this.state.account}
                        balance={this.state.accountBalance}
                        parent={this}
                    />
                </div>
            } else if (1 === this.state.escrowState || 2 === this.state.escrowState) {
                content = 
                <div>
                    <E_EscrowDeposit
                        escrow={this.state.escrow}
                        escrowId={this.state.escrowId}
                        escrowState={this.state.escrowState}
                        account={this.state.account}
                        parent={this}
                    />
                </div>
            } else if (3 === this.state.escrowState) {
                content = 
                <div>
                    <E_EscrowApproval
                        escrow={this.state.escrow}
                        escrowId={this.state.escrowId}
                        escrowState={this.state.escrowState}
                        account={this.state.account}
                        parent={this}
                    />
                </div>
            } else if (4 === this.state.escrowState) {
                content = 
                <div>
                    <E_EscrowWithdrawal
                        escrow={this.state.escrow}
                        escrowId={this.state.escrowId}
                        escrowState={this.state.escrowState}
                        account={this.state.account}
                        parent={this}
                    />
                </div>
            } else {
                content = 
                <div>
                    <E_EscrowDone
                        escrow={this.state.escrow}
                        escrowId={this.state.escrowId}
                        escrowState={this.state.escrowState}
                        account={this.state.account}
                        parent={this}
                    />
                </div>
            }
        }

        return (
        <div>
            <div className="container-fluid">
            <div className="row">
                <div className="col-sm">
                </div>
                <div className="col-sm">
                    <table className="table text-muted text-center">
                        <tbody>
                            <tr>
                                <td style={stleft}><b>Escrow name:</b></td>
                                <td style={stcenter}><p>{this.state.escrowName}</p></td>
                            </tr>
                            <tr>
                                <td style={stleft}><b>Escrow address:</b></td>
                                <td style={stcenter}><p>{this.state.escrowAddress}</p></td>
                            </tr>
                            <tr>
                                <td style={stleft}><b>Escrow id:</b></td>
                                <td style={stcenter}>{this.state.escrowId}</td>
                            </tr>
                            <tr>
                                <td style={stleft}><b>Escrow state:</b></td>
                                <td style={stcenter}>{this.state.escrowState}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div className="col-sm">
                </div>
            </div>
                {content}
            </div>
        </div>
        )
    }
}
export default EscrowBootstrap;