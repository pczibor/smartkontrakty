pragma solidity >=0.6;
import './Escrow.sol';


contract EscrowFactory
{
    uint public nextid;
    mapping (uint => address) addressFromId;
    mapping (address => uint) idFromAddress;

    event escrowCreated(address addr, uint id);

    function initEscrow(string memory name) 
        public
        returns(address, uint)
    {
        ++nextid;
        Escrow escrow = new Escrow(name);
        address escrowAddress = address(escrow);
        addressFromId[nextid] = escrowAddress;
        idFromAddress[escrowAddress] = nextid;
        
        emit escrowCreated(escrowAddress, nextid);
        return (escrowAddress, nextid);
    }

    function getEscrowAddress(uint id)
        public
        view
        returns(address)
    {
        return addressFromId[id];
    }

    function getAllEscrows()
        public
        view
        returns(uint[] memory)
    {
        uint[] memory escrows = new uint[](nextid);
        for (uint i=1; i<=nextid; ++i)
        {
            escrows[i-1] = i;
        }

        return escrows;
    }
}