pragma solidity >=0.6;

contract TimeLock {
    string public name;

    mapping(address => uint256) public balances;
    mapping(address => uint) public funds;
    mapping(uint => address) public locks;
    mapping(uint => uint) public fund_unlocktime;

    event fundsLocked(uint amount, address ownr, uint id);
    event fundsAdded(uint amount, address ownr, uint id);
    event nonExistentFund(address ownr);
    event fundsLocked(uint id);
    event withdrawSuccess(address payable ownr);
    event balanceState(uint balance);

    uint public nextid;
    address payable public owner;

    constructor(string memory _name) 
        payable 
        public
    {
        name = _name;
        owner = msg.sender;
    }

    function lockFunds(uint time)
        payable
        public
    {
        if (balances[msg.sender] > 0)
        {
            balances[msg.sender] += msg.value;
            uint fundid = funds[msg.sender];
            emit fundsAdded(msg.value, msg.sender, fundid);
            return;
        }
        
        nextid++;
        balances[msg.sender] += msg.value;
        funds[msg.sender] = nextid;
        locks[nextid] = msg.sender;
        fund_unlocktime[nextid] = (block.timestamp + time);
        emit fundsLocked(msg.value, msg.sender, nextid);
    }

    function getBalance()
        public
        returns(uint256)
    {
        uint256 balance = balances[msg.sender];
        emit balanceState(balance);
        return balances[msg.sender];
    }

    function withdraw()
        public
    {
        uint uid = funds[msg.sender];
        if (uid > 0)
        {
            uint time = fund_unlocktime[uid];
            if (time > block.timestamp)
            {
                emit fundsLocked(uid);
                return;
            }
            msg.sender.transfer(balances[msg.sender]);
            balances[msg.sender] -= balances[msg.sender]; // =0
            emit withdrawSuccess(msg.sender);
        } 
        else
        { 
            emit nonExistentFund(msg.sender);
        }
    }
}