pragma solidity >=0.6;

contract Escrow 
{
    string public name;
    uint256 public value;
    bool public sellerConfirm = false;
    bool public buyerConfirm = false;

    address payable public seller;
    address payable public buyer;
    mapping (address => uint256) public balances;

    event escrowCrated(string name);
    event sellerDeposited(uint256 amount, address seller);
    event buyerDeposited(uint256 amount, address buyer);
    event confirmSeller();
    event confirmBuyer();
    event completed();
    event aborted();

    enum State {
        Created,
        Await_deposit_seller,
        Await_deposit_buyer,
        Await_confirm,
        Completed,
        Inactive,
        Aborted
    }

    State public state;

    modifier checkState(State s, string memory err) {
        require(state == s, err);
        _;
    }

    modifier onlySeller() {
        require(msg.sender == seller, "Only seller can call this");
        _;
    }

    modifier onlySellerOrBuyer() {
        if (seller == msg.sender || buyer == msg.sender)
        {
            _;
        }
        require(1 == 2, "You can not confirm the state!");
    }

    constructor(string memory _name)
        payable
    {
        name = _name;
        state = State.Await_deposit_seller;
    }

    function getSellerDeposit()
        public
        view
        returns(uint256)
    {

        return balances[seller];
    }

    function getBuyerDeposit()
        public
        view
        checkState(State.Await_confirm, "deposits not yet available")
        returns(uint)
    {
        return balances[buyer];
    }

    function sellerDeposit()
        public
        payable
        checkState(State.Await_deposit_seller, "contract not created")
    {
        value = msg.value/2;
        seller = msg.sender;
        balances[seller] += msg.value;
        state = State.Await_deposit_buyer;

        emit sellerDeposited(value, seller);
    }

    function buyerDeposit()
        public
        payable
        checkState(State.Await_deposit_buyer, "Seller has not deposited yet")
    {
        require((2 * value) == msg.value, "Not enough eth.");
        buyer = msg.sender;
        balances[buyer] += (msg.value/2);
        state = State.Await_confirm;

        emit buyerDeposited(value, buyer);
    }

    function confirmation()
        public
        checkState(State.Await_confirm, "Missing buyers deposit")   
    {
        if (seller == msg.sender)
        {
            sellerConfirm = true;
            emit confirmSeller();
        }

        if (buyer == msg.sender)
        {
            buyerConfirm = true;
            emit confirmBuyer();
        }

        if (buyerConfirm && sellerConfirm)
        {
            state = State.Completed;
            refundBuyer();
        }
    }

    function refundBuyer()
        private
    {
        buyer.transfer(balances[buyer]);
    }

    function withdraw()
        public
        onlySeller
        checkState(State.Completed, "Contract not yet completed")
    {
        state = State.Inactive;
        seller.transfer(3 * value);
    }

    function abort()
        public
        onlySeller   
    {
        if (state <= State.Await_deposit_buyer) {
            state = State.Aborted;
            seller.transfer(value);
            emit aborted();
        }
    }
}