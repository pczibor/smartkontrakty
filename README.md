# Smart kontrakty
# Instalacia

### poziadavky:
 - [nodejs](https://nodejs.org/en/)
 - [npm](https://www.npmjs.com/)
 - [ganache](https://www.trufflesuite.com/ganache)
 - [truffle](https://www.trufflesuite.com/truffle)

### inštalácia
clone repozitara:
```
git clone https://gitlab.com/pczibor/smartkontrakty.git
```

instalacia poziadaviek:
``` npm install ```


# spustenie

 1. zapnut ganache s nastavenim ```quick start```
 2. kompilacia kontraktov: ```truffle delpoy```
 3. interakcia s kontraktom ( vid tests pre priklady) ```truffle console```
 4. pomocou prikazu ```npm start``` sa spusta development server, interakcia mozna pomocou web prehliadaca a Metamask penazenky, stranka sa automaticky otvori

# nedostatky
 1. error reporting by mohol byt konkretnejsi
 2. chyba implemetacia cakania na vytazenie bloku a potvrdenie transakcie
 
